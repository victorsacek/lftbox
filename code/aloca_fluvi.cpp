#include <stdio.h>
#include <stdlib.h>

double **Aloc_matrix_real (long p, long n);
long **Aloc_matrix_long (long p, long n);
double *Aloc_vector_real (long n);
long *Aloc_vector_long (long n);

extern long nodes_max_aloca;

extern double *Df;
extern long *ordem_fluvi;
extern long *direc_fluvi;
extern double *dist_fluvi;
extern double *Qr;
extern double *Qf;
extern long *basins;
extern double *h_min;
extern long *pos_min;

void aloca_fluvi()
{
     Df = Aloc_vector_real (nodes_max_aloca);
     
     ordem_fluvi = Aloc_vector_long (nodes_max_aloca);
     direc_fluvi = Aloc_vector_long (nodes_max_aloca);
     dist_fluvi = Aloc_vector_real (nodes_max_aloca);
     
     Qr = Aloc_vector_real (nodes_max_aloca);
     Qf = Aloc_vector_real (nodes_max_aloca); 
     
     basins = Aloc_vector_long (nodes_max_aloca);
     h_min = Aloc_vector_real (nodes_max_aloca);
     pos_min = Aloc_vector_long (nodes_max_aloca);
     
}
