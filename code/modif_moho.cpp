#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double var_fault_stream(double v);

extern double *moho;
extern double *h_crust_sup;

extern double *h_topo;
extern double *h_bed;
extern double *hlsh;

extern long **conec_Tri;
extern long *pos_conec_Tri;

extern long nodes;

extern double **xy;

extern double dt;
extern double V_meio;

extern double maxx;
extern double minx;
extern double maxy;
extern double miny;

extern double Edge;

extern double seg_per_ano;

extern long **Tri;

extern double axis_stream;

extern double *h_q;

extern double RHOC;
extern double RHOM;
extern double RHOW;

double stream_x (double x,double z);
double stream_z (double x,double z);

int intriangle(double **pontos, long i, long j, long k, double xx, double yy);

extern long Nx;
extern long Ny;

extern double *moho_flex;
extern double *moho_flex_aux;
extern double **xy_flex;

extern double dx_flex;
extern double dy_flex;

extern double **peso;
extern long **peso_pos;

extern double *temper_q;

extern double *beta_crosta;

extern double dt_rift;

extern double nivel;

extern double *h_flex_cumulat;



void modif_moho()
{
    
	
	long i,j,t,verif,cont;
    long k,l,m;
    double x,y,z;
	double x1,x2;
	
	double dx,dz;
    
	double moho_mean,x_mean,y_mean,z_mean,volume;
	
	double zmax=0;
	double moho_max_antes=-35000;
	double moho_max=-35000;
	
	double beta_aux;
	
	double aux_topo;
	double aux_dh;
	
	for (i=0;i<nodes;i++){
		
		x_mean = xy[i][0];
		y_mean = xy[i][1];
		z_mean = moho[i];
		
		//Modifica topografia
		
		aux_topo = h_topo[i];
		aux_dh = (((1.0-1.0/beta_crosta[i])*h_crust_sup[i])/dt_rift)*dt;
		
		if (aux_topo<=nivel && aux_topo+aux_dh<=nivel){
			h_q[i]+=aux_dh*RHOW;
		}
		else {
			if (aux_topo<=nivel)		h_q[i]+=(nivel-aux_topo)*RHOW;
			if (aux_topo+aux_dh<=nivel) h_q[i]+=-(nivel-aux_topo-aux_dh)*RHOW;			
		}
		
		h_topo[i]+=aux_dh;
		h_bed[i]+=aux_dh;
		hlsh[i]+=aux_dh;
		h_flex_cumulat[i]+=aux_dh;
		
		
		//Modifica moho
		
		
		dz = -(z_mean*(1.0-1.0/beta_crosta[i])/dt_rift)*dt;
			
		z = moho[i]+dz;
		
		if (z>h_crust_sup[i]) z=h_crust_sup[i];
		h_q[i]+=-(z-moho[i])*(RHOM-RHOC) -aux_dh*RHOC;
		moho[i]=z;
		
		//if (moho[i]>moho_max) moho_max=moho[i];
	}
	
	//printf("dz_max = %lf  %lf   %lf\n",zmax,moho_max_antes,moho_max);
      
}
