#include <stdio.h>


extern double RHOM;
extern double RHOC;
extern double RHOS;
extern double RHOW;


extern double *h_DT;

extern double w_const_DT1;
extern double w_const_DT2;

extern double t_DT1;
extern double t_DT2;

extern double tempo_flex;

extern long nodes;


extern double x_up;
extern double y_up;
extern double r_up;
extern double t1_up;
extern double t2_up;
extern double w1_up;
extern double w2_up;


extern double *h_up;
extern double *h_up_gauss;


void monta_h_DT()
{
	long i,j;
	
	double tempo_r = tempo_flex/(t_DT2-t_DT1);
	
	double h_aux;
	
	h_aux = (w_const_DT2 - w_const_DT1)*tempo_r*RHOM;
	for (i=0;i<nodes;i++){
		h_DT[i] = h_aux;
	}
	
	printf("t_r = %g\n",tempo_r);
	
	
}


void monta_h_up()
{
	long i,j;
	
	double tempo_r = tempo_flex/(t2_up-t1_up);
	
	double h_aux;
	
	h_aux = (w2_up - w1_up)*tempo_r*RHOM;
	for (i=0;i<nodes;i++){
		h_up[i] = h_aux*h_up_gauss[i];
	}
	
}
