//MODIFICADO  vR modificado aqui!!!! 

#include <stdlib.h>
#include <stdio.h>

void plota_3D(double *hh, long tri,long nodes,double **xy,long **Tri,long *ordem_plota, long cond_print);

extern long **Tri;
extern double **xy;
extern long tri;


extern long *ordem_plota;

extern long nodes;
extern long **conec;
extern long *pos_conec;

extern double *area_vor;
extern double **aresta_vor;
extern double **dist_vor;

extern long *ordem_fluvi;
extern long *direc_fluvi;
extern double *dist_fluvi;

extern double *Qr;
extern double *Qf;

extern double *Df;

extern long *basins;


extern double *h_topo;
extern double *h_bed;
extern double *h_bed_eroded;

extern double *hlsh;

extern double *ls_vec;



extern long max_sil_layers;

extern double **ls_fac; //fator ls em profundidade
extern double **h_ls_fac; //altura da base da silicificação em relação ao embasamento
extern long *ls_fac_num; //indice do topo

extern double h_sil; // initial depth for silicitication
extern double h_sil_max; // maximum depth for silicification

extern double dh_sil; // thickness of silicified layer

extern double tau_R; //

extern double *resistant_fac;


extern double dt;

extern double nivel;

extern double vR;
extern double Kf;
extern double ls;
extern double lb;
extern double lsh;


extern double *h_min;
extern long *pos_min;

extern long *cond_topo_modif;

extern long *stack_fluvi;

extern long n_sub_dt;

extern double *h_topo_prov;

extern double fs_lim;
extern double ls_lambda;

extern double *fac_lb;


extern int n_bed_layers;
extern double *thickness_layer;
extern double *lb_layer;


void fluvi_min()
{
    long i,j,jj;
    double dist_aux;
    long cont_fluvi=nodes;
    double Qeqb;
    long verif,cont_min;

    for (i=0;i<nodes;i++){          
        direc_fluvi[i]=i;
		Qr[i]=dt*area_vor[i]*vR/n_sub_dt;

        Qf[i]=0;   
        Df[i]=0;  
        ordem_fluvi[i]=1;
        if (h_topo[i]>nivel && cond_topo_modif[i]==1){      
            jj=i; 
            dist_aux=100; //qualquer numero;
            for (j=0;j<pos_conec[i];j++){
                if (h_topo[jj]>h_topo[conec[i][j]]){
                    if (aresta_vor[i][j]>0){
                        jj=conec[i][j];
                        dist_aux=dist_vor[i][j];
                    }
                }
            }
            direc_fluvi[i]=jj;
            dist_fluvi[i]=dist_aux;
        }
    }


    for (cont_min=2,i=0;i<nodes;i++){     //determine the drainage basins (1 for sea, >1 for local minima)       
        if (h_topo[i]<nivel || cond_topo_modif[i]==0){
            basins[i]=1;  
        }
        else {
            if (direc_fluvi[i]==i){
                basins[i]=cont_min;
                h_min[cont_min]=1000000.0;
                pos_min[cont_min]=i;                
                cont_min++;
            }
            else basins[i]=0;
        }
    }
    verif=0;
    while (verif==0){
        verif=1;
        for (i=0;i<nodes;i++){
            if (basins[i]==0){
                j = direc_fluvi[i];
                if (basins[j]!=0){
                    basins[i]=basins[j];
                }
                else verif=0;
            }
        }
    }
    
    for (i=0;i<nodes;i++){  //redirect the local minima toward adjacent basins
        if (basins[i]!=1){
            cont_min=basins[i];
            for (j=0;j<pos_conec[i];j++){
                jj=conec[i][j];
                if (basins[jj]==1){
                    if (h_topo[jj]<h_min[cont_min]){
                        //h_min[cont_min]=h_topo[jj]; !!!
                        //direc_fluvi[pos_min[cont_min]]=jj; !!!
                    }
                }
            }
        }
    }
                
                    
    /*for (i=0;i<nodes;i++) Qf[i]=basins[i]*100;
    plota_3D(Qf,tri,nodes,xy,Tri,ordem_plota,2);
    printf("Basins %d", cont_min);       
    for (i=0;i<nodes;i++) Qf[i]=0;*/
    
    
    for (i=0;i<nodes;i++){        
        j = direc_fluvi[i];
        if (j!=i){
            ordem_fluvi[j]=0;                
        }        
    }
	
	long ls_num_aux;
	
	double fs;
	
	
	
    
	/*
    while (cont_fluvi>0){
        
        for (i=0;i<nodes;i++){
            if (ordem_fluvi[i]==1){
                j=direc_fluvi[i];                   
                //if (j!=i && basins[i]!=1){
                if (j!=i){
                    if (h_topo[i]>h_topo[j]){      
                        if (h_topo[j]>nivel)
                            Qeqb = Kf*Qr[i]*(h_topo[i]-h_topo[j])/dist_fluvi[i];
                        else
                            Qeqb = Kf*Qr[i]*(h_topo[i]-nivel)/dist_fluvi[i];
                        if (Qeqb<=Qf[i]){
                            Df[i]=(Qf[i]-Qeqb)/area_vor[i];
                            Qf[j]+=Qeqb;
                        }
                        else {
							Df[i]=((Qf[i]-Qeqb)/area_vor[i])*(dist_fluvi[i]/ls_vec[i]);
							Qf[j]+=Qf[i]+(Qeqb-Qf[i])*(dist_fluvi[i]/ls_vec[i]);
							
							
                        }        
                    }    
                    else {
                        Df[i]=Qf[i]/area_vor[i]; 
                    }      
                    Qr[j]+=Qr[i];
                }
                else {
                    Df[i]=Qf[i]/area_vor[i]; 
                }
                ordem_fluvi[i]=2;
                cont_fluvi--;
                
                //printf("%f\n",Df[i]);
            }
        }
        
        for (i=0;i<nodes;i++){
            if (ordem_fluvi[i]==0){
                ordem_fluvi[i]=1;
            }
        }
        
        for (i=0;i<nodes;i++){
            j = direc_fluvi[i];
            if (ordem_fluvi[i]!=2 && j!=i){
                ordem_fluvi[j]=0;
            }
        }
        
        
    }
	*/
	
	long cont_stack=0;
	
	while (cont_fluvi>0){
		
		for (i=0;i<nodes;i++){
			if (ordem_fluvi[i]==1){
				stack_fluvi[cont_stack]=i;
				cont_stack++;
				j=direc_fluvi[i];
				
				if (j!=i){
					Qr[j]+=Qr[i];
				}
				ordem_fluvi[i]=2;
				cont_fluvi--;
				
				//printf("%f\n",Df[i]);
			}
		}
		
		for (i=0;i<nodes;i++){
			if (ordem_fluvi[i]==0){
				ordem_fluvi[i]=1;
			}
		}
		
		for (i=0;i<nodes;i++){
			j = direc_fluvi[i];
			if (ordem_fluvi[i]!=2 && j!=i){
				ordem_fluvi[j]=0;
			}
		}
		
		
	}
	
	
	
	for (i=0;i<nodes;i++)
		h_topo_prov[i]=h_topo[i];
	
	
	long cont;
	
	for (cont=0;cont<n_sub_dt;cont++){
		
		
		for (i=0;i<nodes;i++){
			if (h_bed[i]<h_topo_prov[i]) {
				if (ls_fac_num[i]>0){
					ls_num_aux = ls_fac_num[i]-1;
					if (h_ls_fac[i][ls_num_aux]>h_topo_prov[i]-h_bed[i]-dh_sil){
						fs = ls_fac[i][ls_num_aux];
						if (fs>fs_lim){
							ls_vec[i]=ls*fs*ls_lambda*resistant_fac[i];
						}
						else {
							ls_vec[i]=ls*(1.0 + (fs*ls_lambda*resistant_fac[i]-1)*(fs-1.0)/(fs_lim-1.0));//ls*fs*lambda;
						}
					}
					else ls_vec[i]=ls;
				}
				else ls_vec[i]=ls;
			}
			else {
				if (fac_lb[i]>0) ls_vec[i]=lb*fac_lb[i];
				else {
					if (n_bed_layers==0) ls_vec[i] = lb_layer[0];
					else {
						int verif_layer=0;
						for (int cont_layers=0;cont_layers<n_bed_layers&&verif_layer==0;cont_layers++){
							if (h_bed_eroded[i]<thickness_layer[cont_layers]){
								ls_vec[i]=lb_layer[cont_layers];
								verif_layer=1;
							}
						}
						if (verif_layer==0) ls_vec[i]=lb_layer[n_bed_layers];
					}
				}
			}
		}
		
		
		for (i=0;i<nodes;i++){
			Qf[i]=0;
		}
		
		
		for (cont_stack=0;cont_stack<nodes;cont_stack++){
			i=stack_fluvi[cont_stack];
			j=direc_fluvi[i];
			
			if (j!=i){
				if (h_topo_prov[i]>h_topo_prov[j]){
					if (h_topo_prov[j]>nivel)
					Qeqb = Kf*Qr[i]*(h_topo_prov[i]-h_topo_prov[j])/dist_fluvi[i];
					else
					Qeqb = Kf*Qr[i]*(h_topo_prov[i]-nivel)/dist_fluvi[i];
					
					if (h_topo_prov[i]<nivel)
					Qeqb = Kf*Qr[i]*(h_topo_prov[i]-h_topo_prov[j])/dist_fluvi[i];
					
					
					if (Qeqb<=Qf[i]){
						Df[i]=(Qf[i]-Qeqb)/area_vor[i];
						Qf[j]+=Qeqb;
					}
					else {
						//if (h_bed[i]<h_topo_prov[i])
						//	lb=ls;
						//else
						//	lb=Lf_vec[i];
						Df[i]=((Qf[i]-Qeqb)/area_vor[i])*(dist_fluvi[i]/ls_vec[i]);
						Qf[j]+=Qf[i]+(Qeqb-Qf[i])*(dist_fluvi[i]/ls_vec[i]);
					}
				}
				else {
					Df[i]=Qf[i]/area_vor[i];
				}
			}
			else {
				Df[i]=Qf[i]/area_vor[i];
			}
			
		}
		
		for (i=0;i<nodes;i++){
			h_topo_prov[i]=h_topo_prov[i]+cond_topo_modif[i]*Df[i];
		}
		
	}
	
	for (i=0;i<nodes;i++)
		Df[i]=h_topo_prov[i]-h_topo[i];
	
	
}




