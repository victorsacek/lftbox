/*
 *  uniform.cpp
 *  FlexErodThermalRift_new_1.0
 *
 *  Created by Victor Sacek on 25/07/13.
 *  Copyright 2013 IAG/USP. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double var_fault_stream(double v);

extern long nodes_thermal;
extern double **xyz_thermal;
extern double **v_adv;

extern double seg_per_ano;
extern double V_meio;

extern double axis_stream;

extern double dx_flex;

double stream_x (double x,double z);
double stream_z (double x,double z);

extern long cont_falha;
extern long num_falha;
extern double **pos_falha_vec;

extern double tempo;

extern double dt_rift;

extern long layers;
extern long Nx;
extern long Ny;
extern long nodes_flex;

extern double *beta_map;

void uniform()
{
	long i,j,k,n,n_ant;
	
	double beta_one;
	
	for (k=0;k<layers;k++){	
		for (i=Nx-1;i>=0;i--){
			for (j=0;j<Ny;j++){
				n = k*nodes_flex+i*Ny+j;
				beta_one = beta_map[i*Ny+j];
				v_adv[n][2] = -xyz_thermal[n][2]*(1.0-1.0/beta_one)/(dt_rift*seg_per_ano);
				if (i!=Nx-1){
					n_ant = k*nodes_flex+(i+1)*Ny+j;
					v_adv[n][0] = -dx_flex*(beta_one-1.0)/(dt_rift*seg_per_ano) + v_adv[n_ant][0];
				}
				else {
					v_adv[n][0] = -dx_flex*(beta_one-1.0)/(dt_rift*seg_per_ano);
				}

			}
		}
	}
	

		
}