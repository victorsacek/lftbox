//
//
// Sem DIFUSAO no CONTINENTE!!!
//
//
////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "header.h"

void malha();
void malha_regular(double minx,double maxx,double miny,double maxy,long Nx, long Ny);

void aloca_difusao();
void aloca_topo();
void aloca_fluvi();

void print_topo(double tempo,long muda_ponto);
long *aloca_plota(long tri,double **xy,long **Tri);
void plota_3D(double *hh, long tri,long nodes,double **xy,long **Tri,long *ordem_plota, long cond_print);
void plota_3D_2(double *hh,double *hh2, long tri,long nodes,double **xy,long **Tri,long *ordem_plota, long cond_print);
void plota_3D_temper(long nodes,double **xy,double *Temper, long cond_print);


void eps_escreve(long **Tri,double **nodv,long tri_aux, long nod,double minx,double miny,double maxx,double maxy);

void difusao();
void modif_topo();

void aloca_fluvi();
void fluvi_min();

void flexuraK();
void flexura_load();

void flexura_solv(double **Kflex, double *Kdiag, double *bflex, double *wflex);

void aloca_calor(double minx,double maxx,double miny,double maxy,double depth, long Nx, long Ny, long Nz);
void calor(double dt_calor);

void monta_h_w();

void monta_h_DT();
void monta_h_up();

void modif_moho();

void free_all();

void calcula_IsoT();

long gera_malha(long n, double **xy, long **Tri);

double var_fault(double v);

void aloca_falhas();
void monta_falha();

void thermal_modiftopo();
void thermal_monta_K();
void thermal_createf();
void thermal_solv(double **Kf, double *Kd, double *b_vec, double *x_vec, double *p, double *r, double *z);
void thermal_renew();
void thermal_firstK1();
void thermal_plot_all();


void uniform();

void fault3D();

double **Aloc_matrix_real (long p, long n);
long **Aloc_matrix_long (long p, long n);
double *Aloc_vector_real (long n);
long *Aloc_vector_long (long n);

void printf_profile();

void aloca_falha_3D();

int main(int argc, char **argv)
{    
    double tempo_max=140.11E6;   
    long i,j;
    double soma_h;
	
	FILE *entra_var;
	entra_var = fopen("param_LFT_layers.txt", "r");
	
	fscanf(entra_var,"%ld",&print_step);
	fscanf(entra_var,"%lf%ld%lf%lf",&dt,&n_sub_dt,&tempo_flex,&dt_calor);
	
	fscanf(entra_var,"%lf",&maxy);
	fscanf(entra_var,"%lf",&miny);
	fscanf(entra_var,"%lf",&perturb);
	fscanf(entra_var,"%ld",&n_lat);
	fscanf(entra_var,"%ld",&n_latx);


	fscanf(entra_var, "%lf",&axis_stream);
	fscanf(entra_var, "%lf",&TeConstante);
	TeConstante2=TeConstante;
	fscanf(entra_var, "%lf",&Telitho);

	fscanf(entra_var,"%lf",&vR);
	fscanf(entra_var, "%lf",&Kf);
	K_d = 0.0;
	fscanf(entra_var, "%lf %lf %lf",&K_m,&K_m_fac,&K_m_fac_depth);
	
	fscanf(entra_var, "%lf",&ls);
	fscanf(entra_var, "%lf",&lb);
	fscanf(entra_var, "%lf",&lsh);
	
	fscanf(entra_var,"%lf",&H_brit);

	
	fscanf(entra_var, "%ld%ld%ld",&Nx,&Ny,&Nz);
	
	fscanf(entra_var, "%lf",&depth);
	
	fscanf(entra_var, "%lf",&dt_rift);
	
	
	fscanf(entra_var,"%lf%lf%lf",&h_sil,&h_sil_max,&dh_sil);
	fscanf(entra_var,"%lf",&tau_R);
	fscanf(entra_var,"%lf",&ls_fac_max);
	fscanf(entra_var,"%lf",&ls_lambda);
	fscanf(entra_var,"%lf",&fs_lim);
	
	fscanf(entra_var,"%d",&n_bed_layers);
	printf("n_bed_layers: %d\n",n_bed_layers);
	if (n_bed_layers>0) thickness_layer = Aloc_vector_real(n_bed_layers);
	lb_layer = Aloc_vector_real(n_bed_layers+1);
	int cont_layers=0;
	for (cont_layers=0;cont_layers<n_bed_layers;cont_layers++){
		fscanf(entra_var,"%lf",&lb_layer[cont_layers]);
		fscanf(entra_var,"%lf",&thickness_layer[cont_layers]);
		
		printf("lb_layer: %lf %lf\n",lb_layer[cont_layers],thickness_layer[cont_layers]);
	}
	fscanf(entra_var,"%lf",&lb_layer[cont_layers]);
	
	
	printf("fs_lim: %lf\n",fs_lim);
	
	
	printf("%lf\n",dt_rift);
	
	h_bot=-depth;
	
	printf("%ld %ld %ld\n", Nx,Ny,Nz);
	layers=Nz;
	
	FILE *f_beta;
	f_beta = fopen("beta.txt","r");
	beta_map = Aloc_vector_real(Nx*Ny);
	for (i=0;i<Nx*Ny;i++){
		fscanf(f_beta,"%lf",&beta_map[i]);
	}
	fclose(f_beta);
	



	aloca_falhas();

		
	fclose(entra_var);

	
	char nome[80];
	FILE *f_perfil;

	
    time_t t1,t2;
    nivel=0.0;
    
    tri_p = Aloc_matrix_long (2000,4);
    cond_p = Aloc_vector_long (2000);
    aresta_p = Aloc_matrix_long (2000,5);
	
	
    malha();
    aloca_topo();
    aloca_falhas();
    aloca_difusao(); 
    aloca_fluvi();
        
    ordem_plota = aloca_plota(tri,xy,Tri);
    ordem_plota_flex = aloca_plota(tri_flex,xy_flex,Tri_flex);
    
    aloca_falha_3D(); 
    
    flexuraK();       
    (void) time(&t1);     
    
    
    for (soma_h=0,i=0;i<nodes;i++){
        soma_h+=h_topo[i]*area_vor[i];
    }
    printf("Area: %f\n",soma_h);
    printf("%f %f %f\n",pos_falha,despl,Edge);
    
    printf("\n"); 
	
	
	printf("m0\n");
    
    tempo=0;
    print_topo(tempo,0);
    monta_falha(); 
	//plota_3D(h_foot,tri,nodes,xy,Tri,ordem_plota,0);
	
	
	//thermal_monta_K(); printf("T1\n");
	
	FILE *f_DT;
	f_DT = fopen("DT_interp.txt","r");
	
	fscanf(f_DT,"%lf",&t_DT1);
	t_DT1*=1.0E6;
	fscanf(f_DT,"%lf",&w_const_DT1);
	
	fscanf(f_DT,"%lf",&t_DT2);
	t_DT2*=1.0E6;
	fscanf(f_DT,"%lf",&w_const_DT2);
	
	monta_h_DT();
	
	
	FILE *mf_DT;
	mf_DT = fopen("uplift.txt","r");
	
	fscanf(mf_DT,"%lf",&x_up);
	fscanf(mf_DT,"%lf",&y_up);
	fscanf(mf_DT,"%lf",&r_up);
	
	double x_aux,y_aux,r_aux;
	for (i=0;i<nodes;i++){
		x_aux = x_up-xy[i][0];
		y_aux = y_up-xy[i][1];
		r_aux = sqrt(x_aux*x_aux+y_aux*y_aux);
		
		h_up_gauss[i] = exp(-(r_aux*r_aux)/(r_up*r_up));
	}
	
	
	fscanf(mf_DT,"%lf",&t1_up);
	t1_up*=1.0E6;
	fscanf(mf_DT,"%lf",&w1_up);
	
	fscanf(mf_DT,"%lf",&t2_up);
	t2_up*=1.0E6;
	fscanf(mf_DT,"%lf",&w2_up);
	
	monta_h_up();
	
	
	
	printf("m1\n");
	
	uniform();
	
	printf("m2\n");
	
	printf_profile();
	
	printf("m3\n");
	
    for (tempo=dt;tempo<tempo_max;tempo+=dt){ 
		
		
		/*if      (tempo<(130.0-120.)*1E6) {vR = 0.5; Kf=0.001;}//0.5;
		else if (tempo<(130.0- 90.)*1E6) {vR = 1.0; Kf=0.002;}//1.0;
		else if (tempo<(130.0- 85.)*1E6) {vR = 1.7; Kf=0.004;}//2.0;
		else if (tempo<(130.0- 55.)*1E6) {vR = 1.5; Kf=0.001;}//1.0;
		else if (tempo<(130.0- 45.)*1E6) {vR = 3.0; Kf=0.008;}//2.0;		
		else if (tempo<(130.0- 33.)*1E6) {vR = 1.5; Kf=0.003;}//1.0;				
		else if (tempo<(130.0- 24.)*1E6) {vR = 1.0; Kf=0.002;}//0.7;
		else if (tempo<(130.0- 12.)*1E6) {vR = 1.5; Kf=0.002;}//1.0;				
		else							 {vR = 1.0; Kf=0.002;}//0.5;*/
		
		if (tempo>t_DT2){
			t_DT1=t_DT2;
			fscanf(f_DT,"%lf",&t_DT2);
			t_DT2*=1.0E6;
			w_const_DT1=w_const_DT2;
			fscanf(f_DT,"%lf",&w_const_DT2);
			
			monta_h_DT();
		}
		
		if (tempo>t2_up){
			t1_up=t2_up;
			fscanf(mf_DT,"%lf",&t2_up);
			t2_up*=1.0E6;
			w1_up=w2_up;
			fscanf(mf_DT,"%lf",&w2_up);
			
			monta_h_up();
		}
		
		
		
		if (cont_falha<num_falha){
			//if (tempo-pos_falha_vec[cont_falha][2]<=0.4E6) Te_uc_on=1;
			if (tempo<=2000.0E6) Te_uc_on=1;
			else Te_uc_on=0;
		}
		
		if (tempo>dt_rift-dt && tempo<dt_rift+dt){
			for (i=0;i<nodes_thermal;i++){
				v_adv[i][0]=0.0;
				v_adv[i][2]=0.0;
			}
		}
        
        
        difusao();    //printf("d");
        fluvi_min();  //printf("f");
        modif_topo(); //printf("M");
        if (long(tempo)%long(dt_calor)==0){
			
			
            thermal_modiftopo(); 
			thermal_monta_K();
			thermal_createf();
			thermal_solv(Kthermal,Kthermal_diag,fthermal,T_vec_fut,p_thermal,r_thermal,z_thermal);
			thermal_renew();
			thermal_firstK1();
			//axis_stream+=V_meio*dt_calor;
        }/**/
        //monta_falha();
        
                    
        if (long(tempo)%long(tempo_flex)==0){

			flexura_load();
			flexura_solv(Kflex, Kdiag, bflex, wflex);    
			
			monta_h_w();
			
			
			printf("Tempo: %f flexura, vR = %f\n",tempo,vR);
			
			for (soma_h=0,i=0,j=0;i<nodes;i++){
                if (soma_h>h_bed[i] && h_topo[i]<nivel){
					soma_h=h_bed[i];
					j=i;
				}
            }
            printf("Tempo: %f Minimo : %g %g\n",tempo,h_bed[j],h_topo[j]);
			
        }/**/
		
		if (tempo<dt_rift){
			modif_moho();
		}
		
		
		fault3D();
		

        
		
	
		
		print_topo(tempo,0);
		
		
        
        if (  (long(tempo)%200000==0 && tempo<=2000000.0) || long(tempo)%500000==0){ 
            free(ordem_plota);
			
            ordem_plota = aloca_plota(tri,xy,Tri);   
			
			for (soma_h=0,j=0;j<Ny;j++){
				for (i=0;i<Nx-1;i++){
					soma_h+=moho_flex[i*Ny+j];
				}
			}
			
            printf("Tempo: %f Volume : %g\n",tempo,soma_h);
        }
		
		if (long(tempo)%1000000==0){
			sprintf(nome,"perfil_FMHE_%.2fMa.txt",tempo/1E6); //topografia
			f_perfil = fopen(nome,"w");
			for (i=0; i<nodes; i++){
				if (xy[i][1]==0)
					fprintf(f_perfil,"%f %f %f\n", xy[i][0], h_topo[i], h_bed[i]);
			}
			fclose(f_perfil);
		}
		
    }
	
	
    
    free(ordem_plota);
    ordem_plota = aloca_plota(tri,xy,Tri);
    
    (void) time(&t2);    
    printf("\nTempo: %ld",t2-t1);
    free_all();
    return(0);   
}

double **Aloc_matrix_real (long p, long n)
{
	double **v;
	long i;
	if(p < 1 || n<1){
		printf("** Erro: Parametro invalido **\n");
		return(NULL);
	}
	v = (double **) calloc(p, sizeof(double *));
	for(i=0; i<p; i++){
		v[i] = (double *) calloc (n, sizeof(double));
		if (v[i] == NULL) {
			printf("** Erro: Memoria Insuficiente **");
			return(NULL);
		}
	}
	return(v);
}
long **Aloc_matrix_long (long p, long n)
{
	long **v;
	long i;
	if(p < 1 || n<1){
		printf("** Erro: Parametro invalido **\n");
		return(NULL);
	}
	v = (long **) calloc(p, sizeof(long *));
	for(i=0; i<p; i++){
		v[i] = (long *) calloc (n, sizeof(long));
		if (v[i] == NULL) {
			printf("** Erro: Memoria Insuficiente **");
			return(NULL);
		}
	}
	return(v);
}
double *Aloc_vector_real (long n)
{
	double *v;
	v = (double *) calloc(n, sizeof(double));
	if (v == NULL){
		printf("* Erro: Memoria Insuficiente *");
		return(NULL);
	}
	return(v);
}

long *Aloc_vector_long (long n)
{
	long *v;
	v = (long *) calloc(n, sizeof(long));
	if (v == NULL){
		printf("* Erro: Memoria Insuficiente *");
		return(NULL);
	}
	return(v);
}
    
