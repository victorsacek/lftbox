void calc_Temper35();

#include <stdio.h>
#include <stdlib.h>

extern FILE *f_h_topo;
extern FILE *f_h_sed;


extern double Sed_esq;
extern double Sed_dir;
extern double Sed_in;

extern long nodes;
extern double **xy;
extern long tri;
extern long **Tri;

extern double *h_topo;
extern double *h_bed;
extern double *h_foot;
extern double *moho;

extern double *h_crust_sup;

extern double *h_flex_cumulat;
extern double *hu_flex_cumulat;

extern double *h_temper35;

extern long nodes_thermal;
extern double **xyz_thermal;
extern double *Temper;

extern double axis_stream;

extern double *ls_vec;

extern double tgdip;
extern double V_meio;
extern double seg_per_ano;

extern double **pos_falha_vec;
extern long cont_falha;
extern long num_falha;

extern long n_lat;
extern long *in_tri;

extern double tempo_max_stream;

extern double tempo_despl;

extern long Te_uc_on;


extern long max_sil_layers;

extern double **ls_fac; //fator ls em profundidade
extern double **h_ls_fac; //altura da base da silicificação em relação ao embasamento
extern long *ls_fac_num; //indice do topo

extern double h_sil; // initial depth for silicitication
extern double h_sil_max; // maximum depth for silicification

extern double dh_sil; // thickness of silicified layer

extern double tau_R; //

extern long print_step;


void print_topo(double tempo,long muda_ponto)
{
    
	calc_Temper35();
	
	long i,j,t;
	
	char nome[30];
	
	if (long(tempo)%print_step==0){
		
		FILE *Ftopo;
		
		sprintf(nome, "m_Topo_%.1f.txt",tempo/1e6);
		
		Ftopo = fopen(nome, "w");
		
		for (i=0;i<nodes;i++) fprintf(Ftopo,"%10.3f %10.3f %10.3f %10.3f %10.4f %6.1f\n",h_topo[i],h_bed[i],h_crust_sup[i],moho[i],h_flex_cumulat[i],h_temper35[i]);
		
		fclose(Ftopo);
		
		sprintf(nome, "m_ls_silica_%.1f.txt",tempo/1e6);
		Ftopo = fopen(nome, "w");
		
		for (i=0;i<nodes;i++){
			t = ls_fac_num[i];
			for (j=0;j<t;j++){
				if (ls_fac[i][j]>1.5){
					fprintf(Ftopo,"%ld %ld %6.3lf %6.3lf\n",i,j,ls_fac[i][j],h_ls_fac[i][j]);
				}
			}
		}
		
		fclose(Ftopo);
		
		
		
		sprintf(nome, "m_ls_top_%.1f.txt",tempo/1e6);
		Ftopo = fopen(nome, "w");
		
		for (i=0;i<nodes;i++) fprintf(Ftopo,"%10.3f\n",ls_vec[i]);
		
		fclose(Ftopo);
		
		
		
		if (tempo==0){
			Ftopo = fopen("m_Tri.txt", "w");
			fprintf(Ftopo, "%ld\n", tri);
			for (t=0;t<tri;t++) fprintf(Ftopo,"%ld %ld %ld\n",Tri[t][0],Tri[t][1],Tri[t][2]);
			fclose(Ftopo);
			
			Ftopo = fopen("m_xy.txt","w");
			fprintf(Ftopo, "%ld\n", nodes);
			for (i=0;i<nodes;i++) fprintf(Ftopo,"%10.3f %10.3f\n",xy[i][0],xy[i][1]);
			fclose(Ftopo);
		}
		
		
		for (i=0;i<nodes;i++){         
			h_flex_cumulat[i]=0; 
			hu_flex_cumulat[i]=0;
		}
				
	}
     
}
