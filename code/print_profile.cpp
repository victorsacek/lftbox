/*
 *  print_profile.cpp
 *  F5_Aden_novo2
 *
 *  Created by Victor Sacek on 27/09/10.
 *  Copyright 2010 IAG/USP. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>

double var_fault(double v);

double **Aloc_matrix_real (long p, long n);
long *Aloc_vector_long(long p);

extern double **xy;
extern double *h_topo;
extern double *moho;

extern long nodes;

extern double tempo;

extern double **pos_falha_vec;
extern long num_falha;

extern double V_meio;

void printf_profile()
{
	double **h3_1,**h3_2;
	long *cont1,*cont2;
	
	h3_1 = Aloc_matrix_real(200, 3);
	h3_2 = Aloc_matrix_real(200, 3);
	
	cont1 = Aloc_vector_long(200);
	cont2 = Aloc_vector_long(200);
	
	long i;
	
	double x,y;
	
	long pos;
	
	for (i=0;i<nodes;i++){
		if (xy[i][1]<100000.0-5000.0){
			x = xy[i][0]-(390000.0+var_fault(xy[i][1]));
			if (tempo<pos_falha_vec[num_falha-1][3])
				x-=tempo*V_meio;
			else 
				x-=pos_falha_vec[num_falha-1][3]*V_meio;
			if (x>0){
				pos = long(x)/7000;
				h3_1[pos][0]+=h_topo[i];
				h3_1[pos][1]+=moho[i];
				cont1[pos]++;
			}
			
		}
		if (xy[i][1]>100000.0+10000.0){
			x = xy[i][0]-(390000.0+var_fault(xy[i][1]));
			if (tempo<pos_falha_vec[num_falha-1][3])
				x-=tempo*V_meio;
			else 
				x-=pos_falha_vec[num_falha-1][3]*V_meio;
			if (x>0){
				pos = long(x)/7000;
				h3_2[pos][0]+=h_topo[i];
				h3_2[pos][1]+=moho[i];
				cont2[pos]++;
			}
		}
	}
	
	char nome[80];
    
	
    FILE *figura;
	
    sprintf(nome,"perfil1_%.2fMa.txt",tempo/1E6); 
    figura = fopen(nome,"w");
	for (i=0;i<200;i++){
		fprintf(figura,"%lf %lf %lf\n",i*7000.0 - 7000.0,h3_1[i][0]/cont1[i],h3_1[i][1]/cont1[i]);
	}
	fclose(figura);
	
	
    sprintf(nome,"perfil2_%.2fMa.txt",tempo/1E6); 
    figura = fopen(nome,"w");
	for (i=0;i<200;i++){
		fprintf(figura,"%lf %lf %lf\n",i*7000.0 - 7000.0,h3_2[i][0]/cont2[i],h3_2[i][1]/cont2[i]);
	}
	fclose(figura);
	
	free(cont1);
	free(cont2);
	free(h3_1);
	free(h3_2);
	
}