double maxx;//600000;
double minx;//200000;
double maxy;//=400000.0;//400000;
double miny;//=0.0;

double Edge;

double perturb;//=0.5;

long n_lat;//=81;
long n_latx;//=101;

long *side;

double *moho_flex;
double *moho_flex_aux;

long print_step;


long **Tri;
long tri;
long tri_max_aloca;
long **conec_Tri;
long *pos_conec_Tri;
long **conec;
long *pos_conec;
double **xy;
long nodes;
long nodes_old;
long nodes_max_aloca;
double *area_vor;
double **aresta_vor;
double **dist_vor;

double *h_topo;
double *h_bed;
double *h_bed_eroded;
double *h_flex_cumulat;
double *hu_flex_cumulat;
double *water;

double *h_DT;
double *h_up;
double *h_up_gauss;

double t_DT1;
double t_DT2;

double w_const_DT1;
double w_const_DT2;

double x_up;
double y_up;
double r_up;
double t1_up;
double t2_up;
double w1_up;
double w2_up;

double *moho;
double *load_Temper;

long *cond_topo_modif;
//double *h_isost;
//double *h_original;

long *stack_fluvi;

long n_sub_dt = 20;

double *h_topo_prov;


///////////////////////////////




///////////////////////////////



long n_falhas;
double **falhas_param;


double *h_q;
double *h_q_a;
double *h_w;

double *h_u;

double *Ds;
double *Df;

long *basins;
double *h_min;
long *pos_min;

long *ordem_fluvi;
long *direc_fluvi;
double *dist_fluvi;

double *Qr;
double *Qf;

double vR;//=1;

double K_d;// = 0.3; /////alterado
double K_m;// = 200.0;
double K_m_fac;
double K_m_fac_depth;
double Kf;// = 0.03;
double ls;// = 10000.0; ////alterado
double lb;// = 100000.0; ////alterado
double lsh;
double nivel;

double *hlsh; //topo do sedimento litificado

double *ls_vec;

long max_sil_layers = 500;

double **ls_fac; //fator ls em profundidade
double **h_ls_fac; //altura da base da silicifica��o em rela��o ao embasamento
long *ls_fac_num; //indice do topo

double *resistant_fac;

double h_sil = 30.0; // initial depth for silicitication
double h_sil_max = 60.0; // maximum depth for silicification

double dh_sil = 20.0; // thickness of silicified layer

double tau_R = 5.0E6; //

double ls_fac_max = 64.;

double ls_lambda = 10.0;

double fs_lim = 2.0;

double *fac_lb;

int n_bed_layers;
double *thickness_layer;
double *lb_layer;




double dt = 2000.0;
double tempo;

long max_conec_p=40;

double topo_seno;


long *ordem_plota;

FILE *f_h_topo;
FILE *f_h_sed;


double dt_rift;// = 10.0E6;

double *beta_crosta;



//////////////////////////////////////////
//Flexura

double TeConstante=5000.0;
double TeConstante2=5000.0;

double Telitho;

long Te_uc_on=1;

long tri_flex;
long **Tri_flex;
long nodes_flex;
double **xy_flex;

long **cond_c;


double **peso;
long **peso_pos;

double **Kflex;

double **Kflex_c;
long **Kconec;
long *Kposconec;

double **Ke;
double *Te;
double *props;
double *Kdiag;
double *Kdiag_c;

//double *qflex;
//double *qflex_a;
double *bflex;
double *wflex;
double *wflex_aux;
double *wflex_cumula;
double *uflex;
double *wflex_fault;

double *Temper35;

double *h_temper35;

double area_ele_flex;

long *ordem_plota_flex;

double *IsoT;

long verif_first=0;

///////////////////////////

double Sed_esq=0;
double Sed_dir=0;
double Sed_in =0;




//////////////////////////

long tetra_thermal;
long **Tet_thermal;
long nodes_thermal;
double **xyz_thermal;
double **xyz_thermal_fut;

double **Kthermal;
double *Kthermal_diag;
double **Mthermal;
double *Mthermal_diag;
double **Kthermal1;
double *Kthermal1_diag;
double **Kthermal2;
double *Kthermal2_diag;

double *T_vec;
double *T_vec_fut;
double *T_vec0;

double *p_thermal;
double *r_thermal;
double *z_thermal;


double *fthermal;

long *cond_thermal;

double **TKe;
double **TMe;

double *Temper;

long thermal_max_conec_p=40;

long **Kthermal_conec;
long *Kthermal_posconec;
long *pos_temper;

double *Dtemper;
long *cond_borda_Temper;
long *cond_borda_Temper2;

double *temper_q;
double *temper_q_a;

double **v_adv;

double *h_top;
double *v_adv2D;

double depth;// = 200000.0;

double h_bot;// = -depth;

double alpha_thermal=0.5;
double comp_alpha_thermal = 1.0 - alpha_thermal;

double H_C;// = 35000.0;
double H_brit;// = 12000.0;

//////////////////////////

double RHOW = 1030;
double RHOS = 2700;
double RHOC = 2700;
double RHOM = 3300;
double alpha_exp_thermo = 3.28E-5;
double kappa = 1.0E-6;
double seg_per_ano = 365.0*24.0*3600.0;


double dt_calor=20000.0;
double dt_calor_sec=dt_calor*seg_per_ano;

double *beta_map;


//////////////////////////

double soma_volume;

long Nx;// = 60;
long Ny;// = 12;
long Nz;// = 12;

long layers;// = Nz;

double dx_flex;
double dy_flex;

double tempo_flex = 4000.0;

double minx_flex;
double miny_flex;

//////////////////////////

double **pos_falha_vec;
long cont_falha=0;
long num_falha;
double pos_falha;
double pos_falha_ant=pos_falha;
double inclina=0.5;

double **falha_plot; //posi��o da falha em y = 0
long **falha_plot_pos;

double tgdip = 1;//1.73;//12.0/20.0;

long *in_tri; // Numero do triangulo para o respectivo ponto novo

//double tempo_muda_falha=500000;

double V_meio=0.01;
double tempo_despl = 2000;
//double despl=20;
double despl=2*V_meio*tempo_despl;


double axis_stream=300000.0;

double tempo_max_stream=40.0E6;

////////////////////////////


double *h_foot;


double *h_crust_sup;

double *moho_aux;


long **tri_p;
long *cond_p;
long **aresta_p;

long ntri_lat;

long **tri_lat;





