#include <stdio.h>
#include <stdlib.h>
#include <math.h>

extern double *h_topo;
extern double *hlsh;
extern double **xy;

extern long max_sil_layers;

extern double **ls_fac; //fator ls em profundidade
extern double **h_ls_fac; //altura da base da silicificação em relação ao embasamento
extern long *ls_fac_num; //indice do topo

extern double h_sil; // initial depth for silicitication
extern double h_sil_max; // maximum depth for silicification

extern double dh_sil; // thickness of silicified layer

extern double tau_R; //

extern double ls_fac_max;


extern double dt;



extern double maxx;
extern double minx;


extern double Sed_esq;
extern double Sed_dir;
extern double Sed_in;

extern double *h_bed;
extern double *h_bed_eroded;
extern double *h_w;
extern double *Ds;
extern double *Df;
extern double *area_vor;
extern long nodes;

extern double *h_foot;

//extern double *h_crust_sup;

extern double *h_q;

extern long *cond_topo_modif;


extern double RHOW;
extern double RHOS;
extern double RHOC;
extern double RHOM;

extern double nivel;


extern double *moho;
extern double *moho_flex;

extern double **peso;
extern long **peso_pos;

extern double *h_crust_sup;

extern double *resistant_fac;

extern double K_m_fac_depth;


void modif_topo()
{
    long i; 
    double aux_bed;  
	double aux_topo;
	double aux_dh;
	double dh;
	
	long ls_num_aux;
	long cond_layer;
	
	double aux_tau_R = dt*log(2.0)/tau_R;
	
    
    for (i=0;i<nodes;i++){   
        
		//if (cond_topo_modif[i]=0) h_topo[i]=0.0;
		
		aux_dh=cond_topo_modif[i]*(Ds[i]+Df[i]);
		aux_topo=h_topo[i];
		aux_bed=h_bed[i];
		
		if (aux_dh>0){
			h_q[i]+=-aux_dh*RHOS;//sedimentacao
		}
		else {
			if (aux_topo==aux_bed) {
				h_q[i]+=-aux_dh*RHOC; //erosao embasamento
			}
			else {
				if (aux_topo+aux_dh<aux_bed) {
					dh = aux_topo-aux_bed;
					h_q[i]+=(dh)*RHOS-(aux_dh+dh)*RHOC;  //erosao embasamento + sedimento
				}
				else {
					h_q[i]+=-aux_dh*RHOS; //erosao sedimento
				}

			}

		}
		
		if (aux_topo<=nivel && aux_topo+aux_dh<=nivel){
			h_q[i]+=aux_dh*RHOW;
		}
		else {
			if (aux_topo<=nivel)		h_q[i]+=(nivel-aux_topo)*RHOW;
			if (aux_topo+aux_dh<=nivel) h_q[i]+=-(nivel-aux_topo-aux_dh)*RHOW;			
		}

			

        h_topo[i]+=aux_dh;

		
        aux_bed=h_bed[i];
        if (aux_bed>h_topo[i]){
			h_bed_eroded[i]+=aux_bed-h_topo[i];
            h_bed[i]=h_topo[i];
            //if (h_foot[i]>h_bed[i]) h_foot[i]=h_bed[i];
        }
		
		aux_bed = h_topo[i]-h_bed[i];
		
		
		cond_layer = 1;
		
		if (h_topo[i]>0.0){
			while (cond_layer==1){
				if (ls_fac_num[i]>0){
					ls_num_aux = ls_fac_num[i]-1;
					if (h_ls_fac[i][ls_num_aux]<aux_bed){ // se a camada silicificada existir, i.e. nao foi erodida
						if (h_ls_fac[i][ls_num_aux]>aux_bed-h_sil_max){
							ls_fac[i][ls_num_aux] += ls_fac[i][ls_num_aux]*aux_tau_R;
							if (ls_fac[i][ls_num_aux]>ls_fac_max)
								ls_fac[i][ls_num_aux]=ls_fac_max;
							cond_layer=0;
						}
						else {
							if (ls_num_aux==max_sil_layers) {
								printf("Numero de camadas silificadas maximo ultrapassado!\n");
								exit(1);
							}
							if (aux_bed - h_sil>0.0){
								h_ls_fac[i][ls_num_aux+1] = aux_bed - h_sil;
								ls_fac[i][ls_num_aux+1] = 1.0;
								ls_fac_num[i]++;
							}
							cond_layer=0;
						}
					}
					else{
						ls_fac_num[i]--;
					}
				}
				else {
					if (aux_bed - h_sil>0.0){
						h_ls_fac[i][0] = aux_bed - h_sil;
						ls_fac[i][0] = 1.0;
						ls_fac_num[i]++;
					}
					cond_layer=0;
				}
			}
		}
	
	
		if (hlsh[i]>h_topo[i]){
			hlsh[i]=h_topo[i];
		}
		if (hlsh[i]<h_topo[i]-K_m_fac_depth){
			hlsh[i]=h_topo[i]-K_m_fac_depth;
		}
	}
	
	
	
	Sed_in=0;
	for (i=0;i<nodes;i++){           
		Sed_in+=(h_topo[i]-h_bed[i])*area_vor[i]/1.0E9;
		if (cond_topo_modif[i]==0){
			if (xy[i][0]<(minx+maxx)/2){
				if (Ds[i]+Df[i]>0){
					Sed_esq+=(Ds[i]+Df[i])*area_vor[i]/1.0E9;
				}
			}
			else {
				if (Ds[i]+Df[i]>0){
					Sed_dir+=(Ds[i]+Df[i])*area_vor[i]/1.0E9;
				}
			}
		}
		
	}
}
