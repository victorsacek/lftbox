#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <time.h>

double **Aloc_matrix_real (long p, long n);
long **Aloc_matrix_long (long p, long n);
double *Aloc_vector_real (long n);
long *Aloc_vector_long (long n);
int incircle(double **xy, long i, long j, long k, long l);
int intriangle(double **pontos, long i, long j, long k, double xx, double yy);
double det (double p1, double p2, double p3, double p4);
void eps_escreve(long **Tri,double **nodv,long tri_aux, long nod,double minx,double miny,double maxx,double maxy);

void malha_regular(double minx,double maxx,double miny,double maxy,long Nx, long Ny);

long gera_malha(long n, double **xy, long **Tri);

void thermal_aloca();
void thermal_modiftopo();
void thermal_monta_K();
void thermal_createf();
void thermal_firstK1();
void thermal_inic();

void plota_3D(double *hh);

extern double maxx;
extern double minx;
extern double maxy;
extern double miny;

extern long n_lat;
extern long n_latx;

extern double Edge;

extern double *h_isost;

extern long **Tri;
extern long tri;
extern long tri_max_aloca;
extern long **conec;
extern long *pos_conec;
extern long **conec_Tri;
extern long *pos_conec_Tri;
extern double **xy;
extern long nodes;
extern long nodes_max_aloca;
extern double *area_vor;
extern double **aresta_vor;
extern double **dist_vor;

extern long max_conec_p;

extern long tri_flex;
extern long **Tri_flex;
extern long nodes_flex;
extern double **xy_flex;

extern double **peso;
extern long **peso_pos;

extern long Nx;
extern long Ny;
extern long Nz;

extern double depth;

extern long *cond_topo_modif;

extern double perturb;

extern long *in_tri; // Numero do triangulo para o respectivo ponto novo

extern double tempo;

void malha()
{
    
    long i,j,jj,maxj,k,l,m,ii;
    double x,y;    
    double area_tri,aresta,aresta_aux;
    double *Ax,*Ay,*B,alpha,beta;
    
    printf("Digite o numero de pontos da lateral\n");
    long n,t;
    //scanf("%d",&n_lat);
    
    n = n_lat*n_latx;
    nodes_max_aloca = n*3;
    if (n<4) {
        printf("Numero invalido de pontos");
        exit(-1);
    }
    nodes=n;
    tri=2*n;
    tri_max_aloca=tri*3;    
    
    
    area_tri = (maxx-minx)*(maxy-miny)/tri;
    //aresta = sqrt(4*1.73*area_tri/3);
    aresta = (maxy-miny)/(n_lat-1);
	
	minx = 100000.0;
	maxx = aresta*(n_latx-1)+minx;
    
    Edge = aresta;
    
    long verif;
    printf("Creating nodes\n");
	
	in_tri = Aloc_vector_long(n_lat);
	
    xy = Aloc_matrix_real(nodes_max_aloca,3);
    conec = Aloc_matrix_long(nodes_max_aloca,max_conec_p);
    pos_conec = Aloc_vector_long(nodes_max_aloca);
    conec_Tri = Aloc_matrix_long(nodes_max_aloca,max_conec_p);
    pos_conec_Tri = Aloc_vector_long(nodes_max_aloca);
    
    cond_topo_modif = Aloc_vector_long (nodes_max_aloca);
    
    dist_vor = Aloc_matrix_real(nodes_max_aloca,max_conec_p);
    
    Ax = Aloc_vector_real(max_conec_p);
    Ay = Aloc_vector_real(max_conec_p);
    B = Aloc_vector_real(max_conec_p);   
    
    area_vor = Aloc_vector_real(nodes_max_aloca);
    aresta_vor = Aloc_matrix_real(nodes_max_aloca,max_conec_p);
    
    Tri= Aloc_matrix_long(tri_max_aloca,4);
	
	
	
	FILE *f_pontos;
	
	f_pontos = fopen("pontos.txt","r");
	
	for (i=0;i<n;i++){
		fscanf(f_pontos, "%lf %lf",&xy[i][0],&xy[i][1]);
		if (xy[i][0]!=minx && xy[i][0]!=maxx){
		//if (xy[i][0]!=maxx){
			cond_topo_modif[i]=1; 
		}
	}
	
	fclose(f_pontos);

    /*xy[0][0]=minx; xy[0][1]=miny;
    xy[1][0]=maxx; xy[1][1]=miny;
    xy[2][0]=maxx; xy[2][1]=maxy;
    xy[3][0]=minx; xy[3][1]=maxy;
    
    i=4;
	
    for (x=minx+aresta;x<maxx-aresta/2;x+=aresta){
        xy[i][0]= x+aresta*perturb*(float)rand()/RAND_MAX-aresta*perturb/2;
        xy[i][1]= miny;  
        cond_topo_modif[i]=1; 
        i++;
    } 
    for (y=miny+aresta;y<maxy-aresta/2;y+=aresta){
        xy[i][0]= maxx;
        xy[i][1]= y+aresta*perturb*(float)rand()/RAND_MAX-aresta*perturb/2;  
        //cond_topo_modif[i]=1;
        i++;
    }
    for (x=maxx-aresta;x>minx+aresta/2;x-=aresta){
        xy[i][0]= x+aresta*perturb*(float)rand()/RAND_MAX-aresta*perturb/2;
        xy[i][1]= maxy;   
        cond_topo_modif[i]=1;
        i++;
    }
    for (y=maxy-aresta;y>miny+aresta/2;y-=aresta){
        xy[i][0]= minx;
        xy[i][1]= y+aresta*perturb*(float)rand()/RAND_MAX-aresta*perturb/2;   
        //cond_topo_modif[i]=1;        
        i++;
    }*/
	
	
    double dist2,cond_dist;
    
    aresta_aux=aresta*aresta*0.7*0.7;
    
    printf("Aresta min: %f\n",sqrt(aresta_aux));
    
    /*for (;i<n;i++){
        printf("\r%d",i);
        xy[i][0]=(maxx-minx)*(float)rand()/RAND_MAX+minx;
        xy[i][1]=(maxy-miny)*(float)rand()/RAND_MAX+miny;
        cond_dist=0;
        for (j=0;j<i && cond_dist==0;j++){
            dist2 = (xy[i][0]-xy[j][0])*(xy[i][0]-xy[j][0]);
            dist2+= (xy[i][1]-xy[j][1])*(xy[i][1]-xy[j][1]);
            //if (sqrt(dist2)<aresta*0.7) cond_dist=1;
            if (dist2<aresta_aux) cond_dist=1;
            
        }
        if (cond_dist==1) i--;
    }
    printf("\n");*/
    srand(1);
    /*for (x=minx+aresta;x<maxx-aresta/2;x+=aresta){
        for (y=miny+aresta;y<maxy-aresta/2;y+=aresta){                
            xy[i][0]= x+aresta*perturb*(float)rand()/RAND_MAX-aresta*perturb/2;
            xy[i][1]= y+aresta*perturb*(float)rand()/RAND_MAX-aresta*perturb/2;
            cond_topo_modif[i]=1;
            i++;
        }
    } */
	
	
	
    printf("%ld %ld\n",i,nodes);
    
    tri = gera_malha(n, xy, Tri);
       
    long aux1;    
    double xi,xj,xk,yi,yj,yk,delta;
    
    
    for (t=0;t<tri;t++){  
        xi=xy[Tri[t][0]][0]; yi=xy[Tri[t][0]][1]; 
    	xj=xy[Tri[t][1]][0]; yj=xy[Tri[t][1]][1]; 
    	xk=xy[Tri[t][2]][0]; yk=xy[Tri[t][2]][1]; 
        delta=0.5*((xj*yk-yj*xk)+xi*(yj-yk)+yi*(xk-xj));  
        if (fabs(delta)<0.01*area_tri) {
            printf("\n\nZero! %ld %ld %ld\n",Tri[t][0],Tri[t][1],Tri[t][2]);
            printf("%f %f\n%f %f\n%f %f\n\n",xi,yi,xj,yj,xk,yk);
            if (t!=tri-1){
                Tri[t][0]=Tri[tri-1][0];
                Tri[t][1]=Tri[tri-1][1];
                Tri[t][2]=Tri[tri-1][2];
                t--; 
            }
            tri--;            
        }
    	if (delta<0) {
    	    aux1=Tri[t][0]; Tri[t][0]=Tri[t][2]; Tri[t][2]=aux1;    		 
        }
    }  
       
    eps_escreve(Tri,xy,t,n,-100000.0,0.0,550000.0,200000.0);
    
    printf("Numero de triangulos %ld\n",tri);
    printf("Numero de nos %ld\n",n);
    printf("t/n = %f", double(t)/double(n));
    
    for (t=0;t<tri;t++){        
        for (i=0;i<3;i++){
            if (i==0) {k=Tri[t][0]; l=Tri[t][1]; m=Tri[t][2];}
            if (i==1) {k=Tri[t][1]; l=Tri[t][2]; m=Tri[t][0];}
            if (i==2) {k=Tri[t][2]; l=Tri[t][0]; m=Tri[t][1];}   
            
            for (verif=0,j=0;j<pos_conec_Tri[k];j++){
                if (conec_Tri[k][j]==t) verif=1;
            }
            if (verif==0){
                conec_Tri[k][pos_conec_Tri[k]]=t;
                pos_conec_Tri[k]++;
            }            
                     
            for (verif=0,j=0;j<pos_conec[k];j++){
                if (conec[k][j]==l) verif=1;
            }
            if (verif==0){
                conec[k][pos_conec[k]]=l;
                pos_conec[k]++;
                if (pos_conec[k]>=max_conec_p){
                    printf("perigo!");                       
                }
            }
            for (verif=0,j=0;j<pos_conec[k];j++){
                if (conec[k][j]==m) verif=1;
            }
            if (verif==0){
                conec[k][pos_conec[k]]=m;
                pos_conec[k]++;
                if (pos_conec[k]>=max_conec_p){
                    printf("perigo!");                  
                }
            }
        }
    }
    
    FILE *f_conec;    
    f_conec = fopen("conec.txt","w");    
    for (i=0;i<n;i++){
        for (j=0;j<pos_conec[i];j++){
            fprintf(f_conec,"%ld ",conec[i][j]);
        }
        fprintf(f_conec,"\n");
    }    
    fclose(f_conec);
    
    double x1,x2,y1,y2,minba,maxba;
    
    double borda_aux=1*aresta;
    
    for (i=0;i<n;i++){
        x1=xy[i][0]; y1=xy[i][1];
        for (j=0;j<pos_conec[i];j++){
            x2=xy[conec[i][j]][0]; y2=xy[conec[i][j]][1];
            Ax[j]=x2-x1;
            Ay[j]=y2-y1;
            B[j]=(x2*x2-x1*x1+y2*y2-y1*y1)/2;
            if (Ax[j]*x1+Ay[j]*y1>B[j]){
                Ax[j]*=-1; Ay[j]*=-1; B[j]*=-1;            
            }
        }
        maxj=j;
        if (x1==minx){Ax[maxj]=-1; Ay[maxj]= 0; B[maxj]=-minx; maxj++;}
        if (x1==maxx){Ax[maxj]= 1; Ay[maxj]= 0; B[maxj]= maxx; maxj++;}
        if (y1==miny){Ax[maxj]= 0; Ay[maxj]=-1; B[maxj]=-miny; maxj++;}
        if (y1==maxy){Ax[maxj]= 0; Ay[maxj]= 1; B[maxj]= maxy; maxj++;}
        
        
        for (j=0;j<maxj;j++){
            minba = 1.0E50; maxba = -1.0E50;
            if (Ax[j]!=0){
                for (jj=0;jj<maxj;jj++){
                    if (jj!=j){
                        alpha=Ay[jj]-Ay[j]*Ax[jj]/Ax[j];
                        beta =B[jj] -B[j] *Ax[jj]/Ax[j];
                        if (alpha>0){
                            if (beta/alpha<minba) minba = beta/alpha;
                        }
                        if (alpha<0){
                            if (beta/alpha>maxba) maxba = beta/alpha;
                        }
                    }
                }
                if (minba-maxba>0) aresta_vor[i][j]=minba-maxba;
                else aresta_vor[i][j]=0;    
                area_vor[i]+=(B[j]/fabs(Ax[j]))*aresta_vor[i][j]/2;  
                aresta_vor[i][j]*=sqrt(Ax[j]*Ax[j]+Ay[j]*Ay[j])/fabs(Ax[j]);     
            }
            else {
                for (jj=0;jj<maxj;jj++){
                    if (jj!=j){
                        alpha=Ax[jj]-Ax[j]*Ay[jj]/Ay[j];
                        beta =B[jj] -B[j] *Ay[jj]/Ay[j];
                        if (alpha>0){
                            if (beta/alpha<minba) minba = beta/alpha;
                        }
                        if (alpha<0){
                            if (beta/alpha>maxba) maxba = beta/alpha;
                        }
                    }
                }
                if (minba-maxba>0) aresta_vor[i][j]=minba-maxba;
                else aresta_vor[i][j]=0;    
                area_vor[i]+=(B[j]/fabs(Ay[j]))*aresta_vor[i][j]/2; 
                aresta_vor[i][j]*=sqrt(Ax[j]*Ax[j]+Ay[j]*Ay[j])/fabs(Ay[j]);            
            }
        }
        
                     
    }
    
    
    for (i=0;i<n;i++){
        x1=xy[i][0]; y1=xy[i][1];
        for (j=0;j<pos_conec[i];j++){
            x2=xy[conec[i][j]][0]; y2=xy[conec[i][j]][1];
            dist_vor[i][j]=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
        }
    }
            
    
    long verif_zero=0;
    
    FILE *f_vor;    
    double soma_area=0;
    f_vor = fopen("voronoi0.txt","w");    
    for (i=0;i<n;i++){
        fprintf(f_vor,"%f %f |",xy[i][0],xy[i][1]);
        fprintf(f_vor,"%f | ",area_vor[i]);
        for (j=0;j<pos_conec[i];j++){
            fprintf(f_vor,"%f ",aresta_vor[i][j]);
            if (aresta_vor[i][j]==0) verif_zero=1;
        }
        if (xy[i][0]==minx || xy[i][0]==maxx || xy[i][1]==miny || xy[i][1]==maxy){
            fprintf(f_vor,"*\n");
        }
        else fprintf(f_vor,"\n");
            
        soma_area+=area_vor[i];
    }  
    fprintf(f_vor,"\n%f",soma_area);  
    fclose(f_vor);
    printf("Verif_zero: %ld\n",verif_zero);
    
    printf("\nArea total calculada:%g",soma_area);///1000000.0);
    printf("\nArea total real:     %g\n",(maxx-minx)*(maxy-miny));///1000000.0);
    
    
    
    double bordaL=5.0;    
    malha_regular(minx-bordaL,maxx+bordaL,miny,maxy,Nx,Ny);   
	thermal_aloca();
	thermal_firstK1();
	thermal_inic();
	
	
	printf("\n");
    
    //malha_regular(minx,maxx*2,miny,maxy,Nx,Ny);    
    //aloca_calor(minx,maxx*2,miny,maxy,depth,Nx,Ny,Nz);
    
    peso = Aloc_matrix_real(nodes_max_aloca,3);
    peso_pos = Aloc_matrix_long(nodes_max_aloca,3);
    
    double a1,a2,a3,b1,b2,b3,c1,c2,c3;
          
          
    printf("Calcula peso: ");  
    for (i=0;i<nodes;i++){
        x = xy[i][0];
        y = xy[i][1];
        if (y>maxy-Edge*0.0001) y-=0.0001*Edge;
        if (y<miny+Edge*0.0001) y+=0.0001*Edge;
        for (verif=0,ii=0;ii<tri_flex && verif==0;ii++){
            if (intriangle(xy_flex,Tri_flex[ii][0],Tri_flex[ii][1],Tri_flex[ii][2],
                           x,y)==0){
                                                  
                peso_pos[i][0]=Tri_flex[ii][0];
                peso_pos[i][1]=Tri_flex[ii][1];
                peso_pos[i][2]=Tri_flex[ii][2];
                
                xi = xy_flex[Tri_flex[ii][0]][0]; 
                yi = xy_flex[Tri_flex[ii][0]][1];
			    xj = xy_flex[Tri_flex[ii][1]][0]; 
                yj = xy_flex[Tri_flex[ii][1]][1];
			    xk = xy_flex[Tri_flex[ii][2]][0]; 
                yk = xy_flex[Tri_flex[ii][2]][1];
                
                delta=0.5*((xj*yk-yj*xk)+xi*(yj-yk)+yi*(xk-xj));
                
                a1=xj*yk-xk*yj; b1=yj-yk; c1=xk-xj;
        		a2=xk*yi-xi*yk; b2=yk-yi; c2=xi-xk;
        		a3=xi*yj-xj*yi; b3=yi-yj; c3=xj-xi;
                		
        		peso[i][0] = (a1+b1*x+c1*y)/2/delta;
        		peso[i][1] = (a2+b2*x+c2*y)/2/delta;
        		peso[i][2] = (a3+b3*x+c3*y)/2/delta;
        		
        		verif=1;
            }            
        }
        if (verif==0){
            printf("out: %f %f %ld\n", x,y,i);
                       
        }
    }
    printf("Fim\n");
    
    
}



int incircle(double **xy, long i, long j, long k, long l)
{
	double Ax,Ay,Bx,By,Cx,Cy,Dx,Dy,aux;
	double M[3][3];
	double Det1,Det2;
	int verif;
	Ax = xy[i][0]; Ay = xy[i][1];
	Bx = xy[j][0]; By = xy[j][1];
	Cx = xy[k][0]; Cy = xy[k][1];
	Dx = xy[l][0]; Dy = xy[l][1];

	Det1 = Ax*By + Ay*Cx + Bx*Cy - By*Cx - Ax*Cy - Ay*Bx;

	if(Det1<0){
		aux=Bx; Bx=Cx; Cx=aux;
		aux=By; By=Cy; Cy=aux;
	}


	M[0][0]=Ax-Dx;
	M[1][0]=Bx-Dx;
	M[2][0]=Cx-Dx;

	M[0][1]=Ay-Dy;
	M[1][1]=By-Dy;
	M[2][1]=Cy-Dy;

	M[0][2]=(Ax-Dx)*(Ax-Dx)+(Ay-Dy)*(Ay-Dy);
	M[1][2]=(Bx-Dx)*(Bx-Dx)+(By-Dy)*(By-Dy);
	M[2][2]=(Cx-Dx)*(Cx-Dx)+(Cy-Dy)*(Cy-Dy);

	Det2 =  M[0][0]*M[1][1]*M[2][2];
	Det2 += M[0][1]*M[1][2]*M[2][0];
	Det2 += M[0][2]*M[1][0]*M[2][1];

	Det2 += -M[0][2]*M[1][1]*M[2][0];
	Det2 += -M[0][0]*M[1][2]*M[2][1];
	Det2 += -M[0][1]*M[1][0]*M[2][2];

	//printf("Det: %g \n",Det2);


	if(Det2<0){
		verif=1;
	}
	else{
		verif=0;
	}

	return(verif);
}

int intriangle(double **pontos, long i, long j, long k, double xx, double yy)
{
	double v[2],v0[2],v1[2],v2[2];
	double a,b;
		
	v[0]  = xx;				                 v[1]  = yy;
	v0[0] = pontos[i][0];				     v0[1] = pontos[i][1];
	v1[0] = pontos[j][0]-pontos[i][0];		 v1[1] = pontos[j][1]-pontos[i][1];
	v2[0] = pontos[k][0]-pontos[i][0];		 v2[1] = pontos[k][1]-pontos[i][1];
	a =  (det(v[0],v2[0],v[1],v2[1])-det(v0[0],v2[0],v0[1],v2[1]))/det(v1[0],v2[0],v1[1],v2[1]);
	b = -(det(v[0],v1[0],v[1],v1[1])-det(v0[0],v1[0],v0[1],v1[1]))/det(v1[0],v2[0],v1[1],v2[1]);
	
    
	if (a>=0 && b>=0 && a+b<=1)	return(0);
	else return(1);		

}
double det (double p1, double p2, double p3, double p4)
{
	return(p1*p4-p2*p3);
}


void eps_escreve(long **Tri,double **nodv,long tri_aux, long nod,double minx,double miny,double maxx,double maxy)
{
	char nome[80];
    
    sprintf(nome,"triangles_%.2fMa.eps",tempo/1E6); //topografia
	
	FILE *epsf;

	epsf = fopen(nome,"w");

	long bxmin=-200,bxmax=800,bymin=-5,bymax=205;
	long k,i;
	double x1,x2,x3,y1,y2,y3;
	double L=maxx-minx;
	char ss0[50],ss1[50],ss2[50];
	

	if (L<maxy-miny) L=maxy-miny;
	

	L=L;

	fprintf(epsf,"%%!PS-Adobe-3.0 EPSF-3.0\n");
	//fprintf(epsf,"%%%%BoundingBox: %ld %ld %ld %ld\n",bxmin,bymin,bxmax,bymax);
	fprintf(epsf,"%%%%BoundingBox: 00 100 400 205\n");
    fprintf(epsf,"1 1 scale\n");
	//fprintf(epsf,"%ld %ld  translate\n",bxmin,bymin);
	fprintf(epsf,"1 setlinejoin\n");
	fprintf(epsf,"0.1 setlinewidth\n");
	fprintf(epsf,"0 setgray\n");
	fprintf(epsf,"/fim {closepath\ngsave\n0.7 setgray\nfill\ngrestore\n} def\n");
	fprintf(epsf,"/mt {moveto} def\n");
	fprintf(epsf,"/lt {lineto} def\n");
	
	fprintf(epsf,"/q {\n a moveto\n b lineto\n c lineto \n closepath\n");
	//fprintf(epsf,"/q {\n 1 d moveto\n 2 d lineto\n 3 d lineto \n closepath\n");
	fprintf(epsf," gsave\n 0.7 setgray\n fill\n grestore\n");
	fprintf(epsf," 0 0 0 setrgbcolor\n stroke\n} def\n");
	
	int base=10;
	
	for (i=0;i<nod;i++){
        x1 = nodv[i][0]/1000;    y1 = nodv[i][1]/1000;
        //x1=(x1-minx)*100/L; y1=(y1-miny)*100/L;
        //modificado///
		fprintf(epsf,"/p%ld {%6.2f %6.2f} def\n",i,x1,y1);
    }
    

	for (k=0;k<tri_aux;k++){
		
    	fprintf(epsf,"/a{p%ld}def /b{p%ld}def /c{p%ld}def q\n",Tri[k][0],Tri[k][1],Tri[k][2]);/**/       

	}
	
	fprintf(epsf, "1 0 0 setrgbcolor\n");
	
	for (i=0;i<n_lat*n_latx;i++){
		fprintf(epsf,"p%ld 1 0 360 arc\n",i);
		fprintf(epsf,"fill\nstroke\n");
	}
	
	fprintf(epsf, "0 1 0 setrgbcolor\n");
	
	for (i=n_lat*n_latx;i<nod;i++){
		fprintf(epsf,"p%ld 0.3 0 360 arc\n",i);
		fprintf(epsf,"fill\nstroke\n");
	}
	
	
	
	fprintf(epsf,"showpage\n");

	

	fclose(epsf);

}
