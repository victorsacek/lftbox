# LFTBox

Landscape, Flexure and Thermal Box

## Steps to use the code:

Compile the code
```bash
cd code
make all
```

Copy the executable to the folder with the example of Figure 8a (Sacek et al., 2019)
```bash
cp LTFBox ../ex_model_Fig8a
```

Run the program on the folder
```bash
cd ../ex_model_Fig8a
./LFTBox
```

Visualize the results
```bash
cp ../python_scripts/plan_view.py ./
python plan_view.py 2 141 2
```


### Section extractor (provisory scripts)
Copy the sec_2D_view.py and sec_strat.py files to the folder with the example of Figure 8a (Sacek et al., 2019)

```bash
cp ../python_scripts/sec* ./
```

Run the Python scripts:
```bash
python sec_strat.py
python sec_2D_view.py
```

