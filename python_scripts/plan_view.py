import sys
import numpy as np
import matplotlib.pyplot as plt


nparam = np.size(sys.argv)-1
if nparam!=3:
	sys.exit("\nERROR. Incorrect number of parameters: %d given.\n3 is the correct number: start, end, step"%(nparam))

start = int(sys.argv[1])
end = int(sys.argv[2])
step = int(sys.argv[3])

x,y = np.loadtxt("m_xy.txt",unpack=True,skiprows=1)
Tri = np.loadtxt("m_Tri.txt",skiprows=1).astype("int")


x=x/1000
y=y/1000


print(np.shape(x))

xmin,xmax = np.min(x),np.max(x)
ymin,ymax = np.min(y),np.max(y)


for i in range(start, end, step):
	h,bed,uc,moho,flex,T = np.loadtxt("m_Topo_%d.0.txt"%(i),unpack=True)
	
	plt.axis("scaled")

	plt.xlim(xmin,xmax)
	plt.ylim(ymin,ymax)
	
	plt.tricontourf(x,y,Tri,h,levels = np.linspace(-800,800,101),extend="both")

	plt.savefig("Topo_%d.png"%(i))



