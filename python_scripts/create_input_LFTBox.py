#Hexa
import numpy as np
from mayavi.mlab import *
import random


nx = 181#151#161 # = 800/5+1	#151
ny = 121#101#161 # = 800/5+1	#21#31

miny =  300000.0
maxy = 1280000.0;

dx = (maxy-miny)/(ny-1);

minx = 100000.0
maxx = dx*(nx-1)+minx

random.seed(1)


perturb = 0.5

x = np.zeros(shape=(nx,ny))
y = np.zeros(shape=(nx,ny))

z = np.zeros(shape=(nx,ny))

fac_lb = np.zeros(shape=(nx,ny))

resistant_fac = z*0.0+1.0

moho = np.zeros(shape=(nx,ny))

hm = 937.5
hb = 812.5


beta_max = 3.0
beta_max_RTJ = 1.3


media_x = (maxx+minx)/2.0

lbeta1=media_x-100000.
lbeta2=media_x+00000.


beta_max_lito = 2.0

lbeta1_lito = media_x-100000.
lbeta2_lito = media_x+100000.




deslocaX = -450e3
deslocaY = 1250e3



f = open('costa.txt','r')

xy = []
cont=0
for line in f:
	cont=cont+1
	line_list = line
	xy.append(map(float, line.split()))

f.close()

nco = cont;
cox = np.zeros(shape=(nco));
coy = np.zeros(shape=(nco));

for i in range(0,cont):
	cox[cont-i-1] = xy[i][0]*1000.*(100./60.)+deslocaX
	#coy[cont-i-1] = -xy[i][1]*1000.*(100./60)+deslocaY#+150000
	coy[cont-i-1] = xy[i][1]*1000.*(100./60)+340000.0

print(np.min(coy),np.max(coy),deslocaY)


delta_x = 10000.
delta_y = 30000.

contTeste=0

beta_crosta = np.zeros(shape=(nx,ny))

for i in range(0,nx):
	for j in range(0,ny):
		if i==0 or i==nx-1:
			x[i][j]=minx+i*dx
		else:	
			x[i][j]=minx+i*dx+dx*perturb*random.random()-dx*perturb/2
			
		if j==0 or j==ny-1:	
			y[i][j]=miny+j*dx
		else:	
			y[i][j]=miny+j*dx+dx*perturb*random.random()-dx*perturb/2
		
		#diffx = x[i][j]-(media_x+(maxx-minx)/5)
		
		#z[i][j] = 800.0*np.exp(-(diffx/50e3)**2)+500 + 400.*np.exp(-((diffx+60e3)/150e3)**2)
		
		xx = x[i][j];
		yy = y[i][j];


		
		for jj in range(0,nco-1):
			if yy>=coy[jj+1] and yy<=coy[jj]:
				inty = (yy-coy[jj])/(coy[jj+1]-coy[jj]);
				xaux = inty*cox[jj+1] + (1-inty)*cox[jj];
				if xaux>xx:
					beta_crosta[i][j] = 1;
				else:
					beta_crosta[i][j] = beta_max - (beta_max - 1.0)*np.exp(-(xx-xaux)/100e3)

	
		
	

		z[i][j] = 200.0;
	
		xcc = (minx+maxx)/2 + (maxx-minx)*(0.1)
		ycc = (miny+maxy)/2 + (maxy-miny)*(-0.11)

		dxcc = xcc-xx
		dycc = ycc-yy

		aaa = 1./.9**2
		bbb = 1./.9**2

		ang = -20.*np.pi/180.

		distcc = np.sqrt(aaa*(dxcc*np.cos(ang)+dycc*np.sin(ang))**2 + bbb*(dxcc*np.sin(ang)-dycc*np.cos(ang))**2)
		
		#distcc = np.sqrt(dxcc*dxcc+4*dycc*dycc)
		z[i][j]=500.0
		
		#if (distcc<350.0E3):
		#	z[i][j]=500.0 #200.+500.*np.exp(-(distcc*2.	/350.0E3)**4.)
			#beta_crosta[i][j]=1 + .5*np.exp(-(distcc*2.	/350.0E3)**4.)
			
		if z[i][j]<450.:
			fac_lb[i][j]=1.
		else:
			fac_lb[i][j]=10.

		moho[i][j]=-34000.0


print(np.min(y),np.max(y))
print(np.min(beta_crosta),np.max(beta_crosta))


figure(figure=None, bgcolor=(1,1,1), fgcolor=(0,0,0), engine=None, size=(800, 700))

#mesh(x,y,z,vmin=-2000,vmax=2000,extent=[minx,maxx,miny,maxy,0.0,90000.0])		

mesh(x,y,beta_crosta,vmin=1,vmax=3)

cc = colorbar();


P_sai = 'topo_moho3.txt'

f1 = open(P_sai,'w')

P_sai = 'pontos.txt'

f2 = open(P_sai,'w')


i=0
j=0
f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')


i=nx-1
j=0
f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')

i=nx-1
j=ny-1
f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')

i=0
j=ny-1
f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')

j=0
for i in range(1,nx-1):
	f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
	f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')
	
i=nx-1
for j in range(1,ny-1):
	f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
	f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')
	
	
j=ny-1
for i in range(nx-2,0,-1):
	f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
	f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')


i=0	
for j in range(ny-2,0,-1):			
	f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
	f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')
	
	
for i in range(1,nx-1):
	for j in range(1,ny-1):
		f1.write(str(z[i][j])+' '+str(moho[i][j])+' '+str(beta_crosta[i][j])+' '+str(resistant_fac[i][j])+' '+str(fac_lb[i][j])+'\n')
		f2.write(str(x[i][j])+' '+str(y[i][j])+'\n')


f1.close()
f2.close()

P_sai = 'param_LFT.txt'

f3 = open(P_sai,'w')
f3.write('2000000\n')
f3.write('2000.0 60 4000.0 20000.0\n')
f3.write(str(maxy)+'\n')
f3.write(str(miny)+'\n')
f3.write(str(perturb)+'\n')
f3.write(str(ny)+'\n')
f3.write(str(nx)+'\n')
f3.write('300000.0\n') #modelo termico (inativo)
f3.write('30000\n') #Te (m)
f3.write('30000\n') #Te (m)
f3.write('1.0\n') #precipitacao (m/ano)
f3.write('0.020\n') #Kf
f3.write('100.0 1. 10.0\n\n') #?
f3.write('10000.0\n') #ls
f3.write('100000.0\n') #lb
f3.write('1000000.0\n') #lb

rNx = 30#33#50
rNy = 20#33#5

f3.write('15000\n') #
f3.write(str(rNx)+' '+str(rNy)+' 15\n') #Nx Ny Nz
f3.write('150000.0\n') #Espessura da litosfera

f3.write('20000000.0\n') #dt_rift

f3.write('1050000000.0 1051000000.0\n')
f3.write('375.0E3 550.0E3\n')
f3.write('250.0E3 150.0E3\n\n')

f3.write('30.0 60.0 20.0\n')
f3.write('5.0E6\n')
f3.write('64.\n')
f3.write('10.0\n')
f3.write('2.0')

f3.close()

xb = np.zeros(shape=(rNx,rNy))
yb = np.zeros(shape=(rNx,rNy))

beta = np.zeros(shape=(rNx,rNy))


P_sai = 'beta.txt'
f3 = open(P_sai,'w')



for i in range(0,rNx):
	for j in range(0,rNy):
		xb[i][j] = i*(maxx-minx)/(rNx-1)+minx
		yb[i][j] = j*(maxy-miny)/(rNy-1)+miny
		
		xx = xb[i][j]
		yy = yb[i][j]

		for jj in range(0,nco-1):
			if yy>=coy[jj+1] and yy<=coy[jj]:
				inty = (yy-coy[jj])/(coy[jj+1]-coy[jj]);
				xaux = inty*cox[jj+1] + (1-inty)*cox[jj];
				if xaux>xx:
					beta[i][j] = 1;
				else:
					beta[i][j] = beta_max - (beta_max - 1.0)*np.exp(-(xx-xaux)/100e3)
					





		
#mesh(xb,yb,beta*5000.0-60000.0)
mesh(xb+1.5e6,yb,beta,vmin=1,vmax=3)
										
for i in range(0,rNx):
	for j in range(0,rNy):				
		f3.write(str(beta[i][j])+'\n')


n = 8
t = np.linspace(-np.pi, np.pi, n)
zp = np.exp(1j * t)
xp = zp.real.copy()*4000
yp = zp.imag.copy()*4000
zp = np.zeros_like(xp)

triangles = [(0, i, i + 1) for i in range(1, n)]
xp = np.r_[0, xp]
yp = np.r_[0, yp]
zp = np.r_[0, zp]
t = np.r_[0, t]


			
view(0,0,6e6)

f3.close()

savefig("beta.png");

