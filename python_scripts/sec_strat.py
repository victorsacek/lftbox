import numpy as np
import matplotlib.tri as mtri
import matplotlib.pyplot as plt

x,y = np.loadtxt("m_xy.txt",unpack=True,skiprows=1)
Tri = np.loadtxt("m_Tri.txt",skiprows=1).astype("int")

x=x/1000
y=y/1000

triang = mtri.Triangulation(x, y, Tri)

x1 = 200.0
y1 = 550.0

x2 = 700.0
y2 = 970.0

N = 200

xi = np.linspace(x1,x2,N)
yi = np.linspace(y1,y2,N)

dist = np.sqrt((x2-x1)**2+(y2-y1)**2)

xx = np.linspace(0,dist,N)

#print(xi)

strat = 2
end = 140
step = 2

camadas=end/strat;

pacotes = np.zeros((N, camadas));

cont_camadas=0
cont=0
c_strat=strat/step
for i in range(0, end+step, step):
	h,bed,uc,moho,flex,T = np.loadtxt("m_Topo_%d.0.txt"%(i),unpack=True)

	sed = h-bed

	interp_lin = mtri.LinearTriInterpolator(triang, h)
	hi = interp_lin(xi, yi)
	
	interp_lin = mtri.LinearTriInterpolator(triang, sed)
	sedi = interp_lin(xi, yi)
	
	interp_lin = mtri.LinearTriInterpolator(triang, bed)
	bedi = interp_lin(xi, yi)
	
	interp_lin = mtri.LinearTriInterpolator(triang, moho)
	mohoi = interp_lin(xi, yi)

	pacotes[:,cont_camadas] = np.copy(sedi)
	for j in range(0,cont_camadas):
		cond = pacotes[:,j]>pacotes[:,cont_camadas]
		pacotes[cond,j] = pacotes[cond,cont_camadas]


	if (cont==c_strat):
		cont=0
		
		np.savetxt("linha_%d.txt"%(i),
				   np.concatenate((
								   np.transpose([hi]),
								   np.transpose([bedi]),
								   np.transpose([sedi]),
								   np.transpose([mohoi]),
								   pacotes),axis=1),fmt="%.2f",
									header="%lf %lf %lf %lf %d %lf %d"%(x1*1000,y1*1000,x2*1000,y2*1000,N,dist*1000,camadas),comments='')
		
		cont_camadas+=1

	cont+=1
