import numpy
import matplotlib.pyplot as plt
from pylab import *
import os




for Uf in range(1,3):
	for Amaxf in range(0,1):
		
		#nome_pasta = "U%.1f"%Uf+"_Amax"+str(int(Amaxf*500))+"_Tempo20.0_sigma600"
		
		#os.chdir(nome_pasta)
		
		if Uf==1:
			f_i = 20
			f_step = 20
			f_f = 140
			fig_name = 'Topo_sec_tudo_20_Myr_thin.eps'
		if Uf==2:
			f_i = 4
			f_step = 4
			f_f = 28
			fig_name = 'Topo_sec_tudo_4_Myr_thin.eps'

		close()

		plt.figure(1, figsize=(6,12))


		"""b1 = [0.1,5./6.,0.8,0.24]
		b2 = [0.1,4./6.,0.8,0.24]
		b3 = [0.1,3./6.,0.8,0.24]
		b4 = [0.1,2./6.,0.8,0.24]
		b5 = [0.1,1./6.,0.8,0.24]
		b6 = [0.1,0./6.,0.8,0.24]
		"""

		cont_f= f_i

		while cont_f<=f_f:

			b1 = [0.15,1.-(cont_f/f_step)*1.0/(7.3),0.8,0.12]
			
			bv1 = plt.axes(b1)

			f = open('linha_'+str(int(cont_f))+'.txt','r')
			a = []
			for line in f:
				line_list = line
				a.append(map(float, line.split()))


			p1x = a[0][0]
			p2x = a[0][2];

			p1y = a[0][1];
			p2y = a[0][3];

			npp = int(a[0][4]);

			dist = a[0][5]/1000;

			camadas = int(a[0][6]);
			
			tam_ad = 2

			h = numpy.zeros(shape=(npp+tam_ad))
			strat = numpy.zeros(shape=(npp+tam_ad))
			emb = numpy.zeros(shape=(npp+tam_ad))
			#lagos = numpy.zeros(shape=(npp))
			sed = numpy.zeros(shape=(npp,camadas))
			x = numpy.zeros(shape=(npp+tam_ad))
			xl = numpy.zeros(shape=(npp))	
			for i in range(0,npp):
				h[i]=a[i+1][0]
				emb[i]=a[i+1][1]
				#lagos[i]=a[i+1][3]*300
				#if h[i]<0:
				#	lagos[i]=300
				#if lagos[i]==0:
				#	lagos[i]=NaN
				x[i]=(dist/(npp-1))*i
				xl[i]=(dist/(npp-1))*i
				for j in range(0,camadas):
					sed[i][j]=a[i+1][j+2]
			f.close()
			
			#plot(xl,lagos/1000,'b',linewidth=4)
			
			x[npp]=x[npp-1]
			x[npp+1]=x[0]
			
			h[npp]=-10000.0
			h[npp+1]=-10000.0
			
			emb[npp]=-10000.0
			emb[npp+1]=-10000.0

			strat[npp]=-10000.0
			strat[npp+1]=-10000.0
			
			for j in range(camadas-1,-1,-1):
				for i in range(0,npp):
					strat[i]=emb[i]+sed[i][j]
				
				rat=1.0*j/camadas
				
				if rat<0.33:
					plot(x,strat/1000.,color=(1,rat*3,0))
					#fill(x,strat/1000.,color=(1,rat*3,0),ec='k')
				if rat>=0.33 and rat<0.66:
					plot(x,strat/1000.,color=(1-(rat-1./3.)*3.,1,0))
					#fill(x,strat/1000.,color=(1-(rat-1./3.)*3.,1,0),ec='k')
				if rat>=0.66:
					plot(x,strat/1000.,color=(0.,1-(rat-2./3.)*3,(rat-2./3.)*3))
					#fill(x,strat/1000.,color=(0.,1-(rat-2./3.)*3,(rat-2./3.)*3),ec='k')
			
			plot(x,h/1000.,'k')
			plot(x,emb/1000.,'k')	
			fill(x,emb/1000.,color=(0.7,0.7,0.7))			

			xlim((0,600))
			ylim((-1.500,1.500))
			
			text(2600,-1.200,str(cont_f)+' Myr',fontsize=15)
			
			ylabel('km')
			
			ax = gca()
			
			if (cont_f<30):
				for xlabel_i in ax.axes.get_xticklabels():
					xlabel_i.set_visible(False)
					xlabel_i.set_fontsize(0.0)
			else:
				xlabel('km')
			
			text(50,-5,str(cont_f)+" Myr  "+str(140-cont_f)+" Ma")

			#if (cont_f==30):
			#	text(400,-0.8,'IA')
			#	text(1300,-0.3,'PA')
			#	text(2500,-0.2,'GA')
			
			cont_f=cont_f+f_step
			
		
		#novo_nome = '../Sec_'+nome_pasta+'.png'
		savefig(fig_name)
		
		close()
		
		#os.rename(fig_name,novo_nome)
		
		#os.chdir("..")

